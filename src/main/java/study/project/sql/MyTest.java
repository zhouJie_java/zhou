package study.project.sql;

import study.project.domain.User;
import study.project.repository.DatabaseUserRepository;

/**
 * @author by zhoujie
 * @date 2021/3/3 19:56
 */
public class MyTest {
    public static void main(String[] args) {
        User user = new User();
        user.setPassword("123");
        user.setName("name");
        DatabaseUserRepository databaseUserRepository = new DatabaseUserRepository(new DBConnectionManager());
        databaseUserRepository.save(user);
    }
}
