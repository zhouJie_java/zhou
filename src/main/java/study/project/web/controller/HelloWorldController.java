package study.project.web.controller;


import study.project.domain.User;
import study.project.mvc.controller.PageController;
import study.project.web.service.LoginService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * 输出 “Hello,World” Controller
 */
@Path("/hello")
public class HelloWorldController implements PageController {

    @Override
    @POST
    @Path("/world") // /hello/world -> HelloWorldController
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Throwable {

        String name = request.getParameter("name");
        String password = request.getParameter("password");
        LoginService loginService = new LoginService();
        User user = new User();
        user.setName(name);
        user.setPassword(password);
        loginService.insert(user);
        return "success.jsp";
    }


}
